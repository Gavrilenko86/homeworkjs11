// Получаем все элементы со страницы, с которыми будем работать
const passwordInputs = document.querySelectorAll('input[type="password"]');
const showPasswordIcons = document.querySelectorAll('.icon-password');
const submitButton = document.querySelector('.btn');
const passwordForm = document.querySelector('.password-form');

// Добавляем обработчики событий на все иконки показа пароля
showPasswordIcons.forEach(icon => {
  icon.addEventListener('click', function() {
    const passwordInput = this.previousElementSibling;
    const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordInput.setAttribute('type', type);
    this.classList.toggle('fa-eye-slash');
  });
});

// Добавляем обработчик события на отправку формы
passwordForm.addEventListener('submit', function(event) {
  event.preventDefault(); // Отменяем стандартное поведение формы

  const passwordInputs = document.querySelectorAll('input[type="password"]');
  const password1 = passwordInputs[0].value;
  const password2 = passwordInputs[1].value;

  // Сравниваем введенные пароли
  if (password1 === password2) {
    alert('You are welcome!');
  } else {
    const errorMessage = document.createElement('span');
    errorMessage.textContent = 'Потрібно ввести однакові значення';
    errorMessage.style.color = 'red';
    passwordInputs[1].insertAdjacentElement('afterend', errorMessage);
  }
});

// Добавляем обработчики событий на все поля ввода пароля для удаления сообщения об ошибке
passwordInputs.forEach(input => {
  input.addEventListener('input', function() {
    const errorMessage = this.nextElementSibling;
    if (errorMessage && errorMessage.tagName.toLowerCase() === 'span') {
      errorMessage.remove();
    }
  });
});
